import { shallowMount } from '@vue/test-utils'
import Manifest from '@/views/Manifest.vue'

var $route = { params: { link : undefined}};
describe('Manifest.vue', () => {
  it('linkifies correctly.', () => {
    var content = "/common/destiny2_content/sqlite/en/world_sql_content_6618f52abda22c46bfd9c7414cc42f9a.content";
    var json = "/common/destiny2_content/json/en/aggregate-168f3d1c-2a06-48e3-b773-1fc465033ab2.json";
    var img = "/common/destiny2_content/json/en/aggregate-168f3d1c-2a06-48e3-b773-1fc465033ab2.jpg";
    var input = {
		"mobileWorldContentPaths": {
			"en": content
		},
		"jsonWorldContentPaths": {
      "en": json
		},
		"icons": {
			"en": img
		}};
    var wrapper = shallowMount(Manifest, {
      mocks: {
        $route
      }
    });
    var result = wrapper.vm.linkify(input);
    expect(result).not.toMatch(`<a href=${wrapper.vm.pageBaseUrl}${content}>"${content}"</a>`);
    expect(result).toMatch(`<a href=${wrapper.vm.pageBaseUrl}${json}>"${json}"</a>`);
    expect(result).toMatch(`<a href=${wrapper.vm.bungieBaseUrl}${img} target="_blank"><img src=${wrapper.vm.bungieBaseUrl}${img} alt=${img}></a>`);
  });
});
